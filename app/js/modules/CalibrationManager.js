const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

class CalibrationManager {
	constructor() {
		this.calibrationMode = false
		this.selectionIndex = 0
		this.calibration = []

		this._size = 50
	}

	selectActiveArea() {
		if(this.calibrationMode)
		{
			$('.active-area').removeClass('selected')
			this.selectionIndex++
			if( this.selectionIndex == $('.active-area').length )
				this.selectionIndex = 0
			$('.active-area').eq( this.selectionIndex ).addClass('selected')
		}
	}

	moveSelectedActiveArea(dir, shift) {
		if(this.calibrationMode)
		{
			let pos = $('.active-area').eq( this.selectionIndex ).offset()
			let multiplier = 1
			if(shift) { multiplier = 50 }

			switch(dir){
				case 'left':
					pos.left -= 1 * multiplier
					break;
				case 'up':
					pos.top -= 1 * multiplier
					break;
				case 'right':
					pos.left += 1 * multiplier
					break;
				case 'down':
					pos.top += 1 * multiplier
					break;
			}

			this.setOffset( pos )
		}
	}
	setOffset( pos ) { $('.active-area').eq( this.selectionIndex ).offset( pos ) }

	scale(dir, shift) {
		if(this.calibrationMode)
		{
			let s = $('.active-area').eq( this.selectionIndex ).width()
			let multiplier = 1
			if( shift ) { multiplier = 10 }

			dir == 'up' ? s += 1 * multiplier : s -= 1 * multiplier

			this.setSize( s )
		}
	}
	setSize(s) {
		$('.active-area').eq( this.selectionIndex ).css({
			width: s + 'px',
			height: s + 'px'
		})
	}


	toggleCalibrationMode() {
		this.calibrationMode = !this.calibrationMode
		this.calibrationMode ? console.log('calibration on') : console.log('calibration off')
	}

	saveCalibration() {
		if(this.calibrationMode)
		{
			this.calibration = []
			let areas = $('.active-area')
			areas.each((i) => {
				this.calibration.push( $( areas[i] ).offset() )
				this.calibration[i].size = $( areas[i] ).width()
			})
			jf.writeFileSync(appRoot.path + '/data/calibration.json', this.calibration)
			console.log('calibration saved', this.calibration)
		}
	}

	resetSelected() { console.log('reset selected') }

	get size() { return this._size }
	set size(v) { this._size = v }
}

module.exports = new CalibrationManager()