const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

const {ipcRenderer} = require('electron')

ipcRenderer.on('broadcast-play', (event, arg) => {
	let source = `<source src="${appRoot.path}/app/video/${arg}" type="video/ogg">`

	$('#videoPlayer').fadeOut({
		duration: 1000,
		progress: (p) => { 
			if(p.tweens[0])
			{
				$('#videoPlayer').find('video')[0].volume = p.tweens[0].now 
			}
		},
		complete: () => {
			$('#videoPlayer').find('video')[0].pause()
			$('#videoPlayer').find('video').find('source').remove()
			$('#videoPlayer').find('video').append( source )
			$('#videoPlayer').find('video')[0].load()
			$('#videoPlayer').find('video')[0].volume = 1
			$('#videoPlayer').find('video')[0].play()
			$('#videoPlayer').fadeIn(1000)
		},
	})
})

$(document).ready( () => {
	$('#videoPlayer').find('video').on('ended', () => {
		$('#videoPlayer').hide()
	})
})