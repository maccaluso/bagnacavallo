// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

// const {dialog} = require('electron').remote
// const win = require('electron').remote.getCurrentWindow()

const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

const UIManager = require('./modules/UIManager')

$(document).ready( () => {
	let data = jf.readFileSync( appRoot.path + '/data/data.json' )
	let calib = jf.readFileSync( appRoot.path + '/data/calibration.json' )
	UIManager.initUI( data, calib )
})