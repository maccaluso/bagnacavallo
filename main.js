const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const shell = electron.shell
const globalShortcut = electron.globalShortcut
const ipcMain = electron.ipcMain

let client
let mainWindow, videoWindow

if( process.env.NODE_ENV === 'dev' )
{
  client = require('electron-connect').client
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
function createWindow () {
  let displays = electron.screen.getAllDisplays()
  let externalDisplay = displays.find((display) => {
    return display.bounds.x !== 0 || display.bounds.y !== 0
  })

  if (externalDisplay) {
    
    mainWindow = new BrowserWindow({
      x: 20,
      y: 20,
      width: 480,
      height: 270,
      frame: false
    })

    mainWindow.loadURL(`file://${__dirname}/app/index.html`)
    if( process.env.NODE_ENV === 'dev' )
    {
      mainWindow.openDevTools()
    }
    else
    {
      mainWindow.setFullScreen(true)
    }

    mainWindow.on('closed', function () { 
      mainWindow = null 
      videoWindow.close()
    })

    videoWindow = new BrowserWindow({
      x: externalDisplay.bounds.x,
      y: externalDisplay.bounds.y,
      width: 1041,
      height: 768,
      frame: false
    })

    videoWindow.loadURL(`file://${__dirname}/app/videoplayer.html`)
    if( process.env.NODE_ENV === 'dev' )
    {
      videoWindow.openDevTools()
    }
    else
    {
      videoWindow.setFullScreen(true)
    }
    videoWindow.on('closed', function () { 
      videoWindow = null 
    })

    mainWindow.focus()
  }
  else
  {
    mainWindow = new BrowserWindow({
      x: 20,
      y: 20,
      width: 640,
      height: 480
    })

    mainWindow.loadURL(`file://${__dirname}/app/index.html`)

    mainWindow.openDevTools()
    
    mainWindow.on('closed', function () { mainWindow = null })

    videoWindow = new BrowserWindow({
      x: 600,
      y: 400,
      width: 320,
      height: 240,
      frame: false
    })

    videoWindow.loadURL(`file://${__dirname}/app/videoplayer.html`)
    videoWindow.openDevTools()
    videoWindow.on('closed', function () { videoWindow = null })

    mainWindow.focus()
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow()

  if( process.env.NODE_ENV === 'dev' )
  {
    client.create( mainWindow, () => {
      console.log('main - electron-connect client created')
    })
    client.create( videoWindow, () => {
      console.log('video - electron-connect client created')
    })
  }

  // // Register a 'CommandOrControl+X' shortcut listener.
  const ret = globalShortcut.register('CommandOrControl+F', () => {
    mainWindow.setFullScreen(true)
  })

  const esc = globalShortcut.register('Escape', () => {
    mainWindow.setFullScreen(false)
  })

  const reload = globalShortcut.register('CommandOrControl+R', () => {
    mainWindow.reload()
  })

  const quit = globalShortcut.register('CommandOrControl+Q', () => {
    app.quit()
  })

  // if (!ret) {
  //   console.log('registration failed')
  // }
})

app.on('will-quit', () => { globalShortcut.unregisterAll() })

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('play', (event, arg) => {
  videoWindow.send('broadcast-play', arg)
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.